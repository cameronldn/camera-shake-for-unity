# Camera Shake for Unity

Camera Shake for Unity is a simple tool to give your camera or object dynamic shake and vibration. 

It can be applied to any object in your scene and accessed quickly and easily via a static class to give a dynamic shake effect.